package com.shuneault.agdq2016.alarms;

import com.shuneault.agdq2016.AppSingleton;
import com.shuneault.agdq2016.R;
import com.shuneault.agdq2016.intents.ExternalIntents;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

/**
 * Created by sebast on 29/07/15.
 */
public class ScheduledService extends IntentService {

    public ScheduledService() {
        super("SGDQ Alarm");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        PendingIntent twitchPending = PendingIntent.getActivity(this, 0, new ExternalIntents(this).getTwitchIntent(), PendingIntent.FLAG_ONE_SHOT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("M_CH_ID", "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(false);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "M_CH_ID");
        builder.setContentText(getResources().getString(R.string.app_name));
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setContentTitle(intent.getStringExtra("Game") + " is currently playing");
        builder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.FLAG_ONLY_ALERT_ONCE);
        builder.setTicker(intent.getStringExtra("Game"));
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.addAction(R.drawable.ic_twitch, "Watch on Twitch", twitchPending);
        builder.setAutoCancel(true);
        Notification notification = builder.build();
        notificationManager.notify(intent.getIntExtra("GameId", 0), notification);

        // Delete the alarm
        Intent i = new Intent(this, ScheduledService.class);
        intent.putExtra("Game", intent.getStringArrayExtra("Game"));
        intent.putExtra("GameId", intent.getIntExtra("GameId", 0));
        PendingIntent sender = PendingIntent.getService(this, intent.getIntExtra("GameId", 0), i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager mgr = (AlarmManager) this.getSystemService(ALARM_SERVICE);
        mgr.cancel(sender);
    }
}
