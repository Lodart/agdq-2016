package com.shuneault.agdq2016.settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.shuneault.agdq2016.R;

/**
 * Created by sebast on 03/01/16.
 */
public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

}
