package com.shuneault.agdq2016;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.shuneault.agdq2016.objects.GameSchedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    private AppSingleton mSingleton;

    public ApplicationTest() {
        super(Application.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        mSingleton = AppSingleton.getInstance(getContext());
    }

    public void testWebSourceCode() throws Exception {
        WebCodeSource th = new WebCodeSource(new WebCodeSource.OnThreadProgress() {
            @Override
            public void onProgress(Integer progress) {

            }

            @Override
            public void onFinish(ArrayList<GameSchedule> gameSchedule) {
                Log.i("LOGCAT", "Game Size: " + gameSchedule.size());
                assertEquals(1234, gameSchedule.size());
            }
        });
        th.execute();
    }

    public void testSharedPrefs() throws Exception {
        mSingleton.clearReminders();
        mSingleton.addReminder(new GameSchedule("Custom", Calendar.getInstance(), "Runner", "Run Time", "Category", "Setup Time", "Description"));
        assertEquals(1, mSingleton.getReminders().size());
        mSingleton.saveReminders();
        mSingleton.loadReminders();
        assertEquals(1, mSingleton.getReminders().size());
        assertEquals("Custom", mSingleton.getReminders().get(0).getName());
    }

    public void testReminders() throws Exception {
        Log.d("LOGCAT", "TEST: REMINDERS: " + mSingleton.getReminders().keySet().toString());
    }
}